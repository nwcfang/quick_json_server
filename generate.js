/**
 * Created by Mikhail Gusev on 24.02.16.
 */

module.exports = function() {
    var faker = require('faker');
    var _ = require('lodash');
    return {
        projects: _.times(100, function(n){
            return {
                id: n,
                name: faker.company.companyName()
            }
        })
    }
};
